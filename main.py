import tensorflow as tf
import cv2
import numpy as np  
import onnxruntime
from decode_crnn import CTC_Decode

class OCR_ONNX:
    def __init__(self,pth_model="./resources/model_idcard.onnx", pth_char_list="./resources/extra_char_list.txt"):
        self.sess = onnxruntime.InferenceSession(pth_model)
        self.CHAR_PATH = pth_char_list
        self.ctc_decoder = CTC_Decode(self.CHAR_PATH)
    
    def predict(self,img, shape_input =[512,32,1]):
        img, steps = self.pre_process_img(img)
        lengths_arr = np.array([[steps]], dtype=np.int32)
        img = np.array(img).reshape(shape_input)
        pred = self.sess.run(None, {'input_length': lengths_arr, 'the_input': [img]})
        labels, prob_seq = self.ctc_decoder.ctc_decode(pred[0], [steps])
        pred_text = self.ctc_decoder.label2text(labels=labels)
        return pred_text, prob_seq
    
    def pre_process_img(self, img, fixed_size=32):
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        h, w = img.shape
        fx = fixed_size / h
        fy = fx
        img = cv2.resize(img, None, fx=fx, fy=fy)
        h, w = img.shape[:2]
        img = cv2.copyMakeBorder(img, 0, 0, 0, 512-w, cv2.BORDER_CONSTANT, None, value = 255)
        img = img.astype(np.float32)
        img /= 255
        steps = 512//4 - 1
        return img.T, steps




ocr_onnx = OCR_ONNX()
img = cv2.imread("./Capture.PNG")
import time
st = time.time()
pred_text, prob = ocr_onnx.predict(img)
print(time.time() - st)
# print(pred_text)
# print(prob)
# ocr_onnx.decode_CTC(pred_text)

        




