import numpy as np
import tensorflow as tf
from tensorflow.python.ops import ctc_ops as ctc
from keras import backend as K


class CTC_Decode:
    def __init__(self, pth_charlist):
        self.charlist = self.read_char_list(pth_charlist)
    @staticmethod
    def read_char_list(pth):
        chars = []
        with open(pth) as fi:
            for line in fi:
                char = line.strip()
                if char:
                    if len(char) > 1:
                        raise ValueError("Character {} in {} is invalid.".format(char, pth_charlist))
                    else:
                        chars.append(char)
                        if char == '9':
                            chars.append(' ')
    
        return chars
    
    def label2text(self,labels):
        res = []
        for c in labels:
            if c == len(self.charlist):
                res.append("#")
            else:
                res.append(self.charlist[c])
        return "".join(res)

    def ctc_decode(self, y_pred, input_length, greedy=True, beam_width=100,
               top_paths=1):
        y_pred_copy = np.copy(y_pred)
        y_pred = tf.math.log(y_pred + K.epsilon())
        y_pred = tf.transpose(y_pred, perm=[1, 0, 2])
        input_length =  tf.cast(input_length, tf.int32)
        if greedy:
            (decoded, log_prob) = ctc.ctc_greedy_decoder(
                inputs=y_pred,
                sequence_length=input_length)
        else:
            (decoded, log_prob) = ctc.ctc_beam_search_decoder(
                inputs=y_pred,
                sequence_length=input_length, beam_width=beam_width,
                top_paths=top_paths,
                merge_repeated=False
            )

        out_bests =  [i for i in np.array(decoded[0].values)]
        out_bests = [out_bests]
        steps = 512//4 - 1
        np_lengths = np.array([steps])
        np_lengths = np.expand_dims(np_lengths, axis=-1)
        label_lenght = np.zeros((1,1), dtype=np.int32)

        for i, outputbest in enumerate(out_bests):
            if (len(outputbest) !=0):
                label_lenght[i] = len(outputbest)
            else:
                label_lenght[i] = 1
                out_bests[i] = [len(self.charlist)]
        
        max_label_lenghs = label_lenght.max()
        np_out_bests = np.ones([1, max_label_lenghs], dtype=np.int32) * -1
        for i, out_best in enumerate(out_bests):
            np_out_bests[i][:len(out_best)] = out_best

        confidenceNet = ConfidenceNet()
        loss = confidenceNet.__call__(np_out_bests, y_pred_copy, np_lengths, label_lenght)
        loss = np.squeeze(loss, -1)
        prob_seq = np.exp(-loss)
        prob_seq = [min(i+0.14, 0.98) for i in prob_seq]
        return np.array(decoded[0].values), prob_seq
            
        
class ConfidenceNet:
    def __init__(self):
        self.graph = tf.compat.v1.Graph()
        with self.graph.as_default():
            self.sess = tf.compat.v1.Session()

            self.y_true = tf.compat.v1.placeholder(dtype=tf.int32, shape=(None, None), name="y_true")
            self.y_pred = tf.compat.v1.placeholder(dtype=tf.float32, shape=(None, None, None), name="y_pred")
            self.input_length = tf.compat.v1.placeholder(dtype=tf.int32, shape=(None, 1), name="input_length")
            self.label_length = tf.compat.v1.placeholder(dtype=tf.int32, shape=(None, 1), name="label_length")

            self.loss = self.ctc_batch_cost(self.y_true, self.y_pred, self.input_length, self.label_length)

    
    def ctc_batch_cost(self,y_true, y_pred, input_length, label_length):
        label_length = tf.cast(tf.squeeze(label_length, axis=-1), tf.int32)
        input_length = tf.cast(tf.squeeze(input_length, axis=-1), tf.int32)
        sparse_labels = tf.cast(K.ctc_label_dense_to_sparse(y_true, label_length), tf.int32)

        y_pred = tf.math.log(tf.transpose(y_pred, perm=[1, 0, 2]) + K.epsilon())

        return tf.expand_dims(ctc.ctc_loss(inputs=y_pred,
                                       labels=sparse_labels,
                                       sequence_length=input_length,
                                       ignore_longer_outputs_than_inputs=True), 1)

    
    def __call__(self, y_true, y_pred, input_length, label_length):
        """
        :param y_true:
        :param y_pred:
        :param input_length:
        :param label_length:
        :return:
        """
        with self.graph.as_default():
            return self.sess.run(self.loss, feed_dict={
                self.y_true: y_true,
                self.y_pred: y_pred,
                self.input_length: input_length,
                self.label_length: label_length,
            })
